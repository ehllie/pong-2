extends RigidBody2D

var reset = false
var speed = 300

func _ready():
	visible = true
	randomize()
	reset = true


func _integrate_forces(state):
	if reset:
		state.transform = Transform2D(0, Vector2(640, 360))
		var mag = randf() * 0.8
		state.linear_velocity = Vector2([-1, 1][randi() % 2], [-0.8, 0.8][randi() % 2] * (mag + 0.2)).normalized() * speed
		reset = false

func stop():
	visible = false
