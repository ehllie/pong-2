extends RigidBody2D

var pushing_up = false
var pushing_down = false
var reset = false

func _ready():
	reset = true
	visible = true

func move_home(state):
	var dist = (get_parent().PlayerHome - position.x) / 5
	state.apply_central_impulse(get_parent().ReturnVector * dist * abs(dist))
	
func align(state):
	var tilt = rotation_degrees
	state.apply_torque_impulse(tilt * abs(tilt) * get_parent().AlignTorque)

func push_up(yes, state):
	if yes:
		state.add_central_force(Vector2.ZERO if pushing_up else get_parent().MoveVector * -1)
		pushing_up = true
	else:
		state.add_central_force(get_parent().MoveVector if pushing_up else Vector2.ZERO)
		pushing_up = false
		
func push_down(yes, state):
	if yes:
		state.add_central_force(Vector2.ZERO if pushing_down else get_parent().MoveVector)
		pushing_down = true
	else:
		state.add_central_force(get_parent().MoveVector * -1 if pushing_down else Vector2.ZERO)
		pushing_down = false

func _integrate_forces(state):
	if reset:
		state.transform = Transform2D(0, Vector2(get_parent().PlayerHome, 360))
		state.linear_velocity = Vector2.ZERO
		state.angular_velocity = 0
		reset = false
	push_up(Input.is_action_pressed("ui_up"), state)
	push_down(Input.is_action_pressed("ui_down"), state)
	move_home(state)
	align(state)

func stop():
	visible = false
