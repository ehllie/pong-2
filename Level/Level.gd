extends Node

var PlayerHome = 35
var OpponentHome = 1245
var MoveVector = Vector2(0, 1) * 350
var ReturnVector = Vector2(1, 0)
var AlignTorque = -.7
var AIThreshold = 35

var PlayerScore = 0
var OpponentScore = 0

func pause_game():
	$ScoreSound.play()
	get_tree().call_group("GameElems", "stop")
	$CountdownTimer.start()
	$Countdown.show()
	
func _process(_delta):
	if not $Player.pushing_down and Input.is_action_pressed("ui_down"):
		$Player.apply_central_impulse(MoveVector*0.1)
	if not $Player.pushing_up and Input.is_action_pressed("ui_up"):
		$Player.apply_central_impulse(MoveVector*-0.1)
	$PlayerScore.text = str(PlayerScore)
	$OpponentScore.text = str(OpponentScore)
	$Countdown.text = "%1.1f" % $CountdownTimer.time_left

func _on_Ball_body_entered(body):
	if body.name == "AreaLeft":
		OpponentScore += 1
		pause_game()
	elif body.name == "AreaRight":
		PlayerScore += 1
		pause_game()
	else:
		$CollisionSound.play()
		


func _on_AreaLeft_body_entered(body):
	if body.name == "Ball":
		OpponentScore += 1
		pause_game()


func _on_AreaRight_body_entered(body):
	if body.name == "Ball":
		PlayerScore += 1
		pause_game()


func _on_CountdownTimer_timeout():
	$Countdown.hide()
	get_tree().call_group("GameElems", "_ready")
